# predictions for test data used for (possible) blending
---
- Here goes raw prediction in a format of np.arrays 
<br>of shape (n_samples_test, num_classes) = <br>
(4376, 3) or other dimensions, squeezable to given <br>
to allow class probabilities blending

- The array can be read back with pickle.load or numpy.load.