# Models, (ex|im)plicitly used to produce features
---
- FastText (32 dim embedding), trained on accepted functions/products
### to save model
```python
from os.path import join as pjoin

MODEL_DIR = pjoin('..', 'models')
ft.save(pjoin(MODEL_DIR, 'ft_model.wv'))
```
### to load model back
```python
from gensim.models import FastText

MODEL_DIR = pjoin('..', 'models')
ft = FastText.load(pjoin(MODEL_DIR, 'ft_model.wv'))
```
